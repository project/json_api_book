<?php

namespace Drupal\json_api_book\EventSubscriber;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\book\BookManagerInterface;
use Drupal\jsonapi\Routing\Routes;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Event subscriber which alters JSON:API responses for book related nodes.
 */
class ResponseSubscriber implements EventSubscriberInterface {

  /**
   * The route match service.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The book manager service.
   *
   * @var \Drupal\book\BookManagerInterface
   */
  protected BookManagerInterface $bookManager;

  /**
   * An array with already loaded book definitions.
   *
   * @var array
   */
  protected array $loadedBookDefinitions = [];

  /**
   * An array of already loaded UUIDs.
   *
   * @var array
   */
  protected array $loadedUUIDs = [];

  /**
   * An array of already loaded Urls.
   *
   * @var array
   */
  protected array $loadedUrls = [];

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|null
   */
  protected ?EntityStorageInterface $nodeStorage = NULL;

  /**
   * ResponseSubscriber constructor.
   *
   * @param \Drupal\book\BookManagerInterface $bookManager
   *   The book manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(BookManagerInterface $bookManager, EntityTypeManagerInterface $entityTypeManager) {
    $this->bookManager = $bookManager;
    try {
      $this->nodeStorage = $entityTypeManager->getStorage('node');
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      // This is just theoretical and should never happen.
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::RESPONSE] = ['onResponse'];
    return $events;
  }

  /**
   * Set route match service.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The  route match service.
   */
  public function setRouteMatch(RouteMatchInterface $route_match): void {
    $this->routeMatch = $route_match;
  }

  /**
   * Called when KernelEvents::RESPONSE event is dispatched.
   *
   * If the response contains content from the JSON:API, it checks if any of the
   * records are nodes to find optional book definitions for those nodes adding
   * them as the new attribute 'drupal_internal__book' to each applicable
   * record.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The filter event.
   */
  public function onResponse(ResponseEvent $event): void {
    if ($this->nodeStorage === NULL || !$this->routeMatch->getRouteObject()) {
      return;
    }
    if ($this->routeMatch->getRouteName() === 'jsonapi.resource_list' || Routes::isJsonApiRequest($this->routeMatch->getRouteObject()->getDefaults())) {
      $response = $event->getResponse();
      $content = $response->getContent();
      if (!$content) {
        return;
      }
      try {
        $jsonapi_response = json_decode($content, TRUE, 512, JSON_THROW_ON_ERROR);
      }
      catch (\JsonException) {
        return;
      }
      if (!is_array($jsonapi_response)) {
        return;
      }
      if (isset($jsonapi_response['data']['attributes']['drupal_internal__nid'])) {
        // The response contains one distinct node.
        $this->findBookDefinition($jsonapi_response['data']['attributes']);
      }
      elseif (isset($jsonapi_response['data'][0]['attributes']['drupal_internal__nid'])) {
        // The response contains a list of nodes.
        foreach ($jsonapi_response['data'] as $key => $values) {
          $this->findBookDefinition($jsonapi_response['data'][$key]['attributes']);
        }
      }
      try {
        $content = json_encode($jsonapi_response, JSON_THROW_ON_ERROR);
      }
      catch (\JsonException) {
        return;
      }
      $response->setContent($content);
    }
  }

  /**
   * Helper function called by self::onResponse to find book definitions.
   *
   * @param array $record
   *   The 'attributes' array of a single node record containing all the details
   *   provided by JSON:API.
   */
  private function findBookDefinition(array &$record): void {

    // The node ID for which the book definitions should be loaded.
    $nid = $record['drupal_internal__nid'];

    // If we haven't processed the given node ID yet, let's do it now.
    if (!isset($this->loadedBookDefinitions[$nid])) {
      // Get the book definition for the given node ID from Drupal core's
      // book manager service.
      $books = $this->bookManager->loadBookLinks([$nid], FALSE);
      // Save the result in the local list of already processed node IDs.
      $this->loadedBookDefinitions[$nid] = reset($books);
      // If the given node belongs to a book, find further details for the
      // book definitions, i.e. the UUIDs of all the referenced nodes.
      if (!empty($this->loadedBookDefinitions[$nid])) {
        // Each book definition has at least their own id, a book id and a
        // parent id.
        $references = ['nid', 'bid', 'pid'];
        // In addition, each record has up to 9 additional parent ids, depending
        // on the depth within the book.
        for ($i = 1; $i <= $this->loadedBookDefinitions[$nid]['depth']; $i++) {
          $references[] = 'p' . $i;
        }
        // Load the UUID for each referenced node.
        foreach ($references as $reference) {
          $refId = $this->loadedBookDefinitions[$nid][$reference];
          if (!isset($this->loadedUUIDs[$refId])) {
            /** @var \Drupal\node\NodeInterface|null $node */
            $node = $this->nodeStorage->load($refId);
            if ($node === NULL) {
              continue;
            }
            $this->loadedUUIDs[$refId] = $node->uuid();
            $route_name = sprintf('jsonapi.%s--%s.individual', 'node', $node->bundle());
            $this->loadedUrls[$refId] = Url::fromRoute($route_name, ['entity' => $node->uuid()])->setOption('absolute', TRUE)->toString();
          }
          // Add the UUID of the referenced node to the book definition.
          $this->loadedBookDefinitions[$nid][$reference . '__uuid'] = $this->loadedUUIDs[$refId];
          $this->loadedBookDefinitions[$nid][$reference . '__url'] = $this->loadedUrls[$refId];
        }
      }
    }

    // If the given node applies to a book, add its definition to the record
    // which is being sent back to the client by the JSON:API.
    if (!empty($this->loadedBookDefinitions[$nid])) {
      $record['drupal_internal__book'] = $this->loadedBookDefinitions[$nid];
    }
  }

}
